package ar.edu.unnoba.ppc2016.juego;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import ar.edu.unnoba.ppc2016.R;

public class JuegoActivity extends AppCompatActivity implements View.OnTouchListener{

    private static final int DIFICULTAD_FACIL = 1;
    private static final int DIFICULTAD_MEDIA = 2;
    private static final int DIFICULTAD_DIFICIL = 3;
    private static final int OP_MAYOR_MENOR = 0;
    private static final int OP_SUMA = 1;
    private static final int OP_RESTA = 2;

    CountDownTimer contador;
    TextView txtPrimerOp, txtSegundoOp, txtResultado, txtSigno, txtSeleccionado, txtIgual, txtPuntaje, txtTiempo, txtPantalla;
    GridLayout grid;
    ArrayList<Integer> opciones; // opciones a cargar en el grid
    ArrayList<Point> posiciones; //posiciones ocupadas por las opciones del grid
    int textSize, width, height ;// para setear un textSize dependiendo la pantalla
    Random rnd;
    boolean primeroCompleto, segundoCompleto, resultadoCompleto;
    boolean completo= false; //completo indica que se completaron todos los campos  y se cambia de pantalla
    int tipoOperacion, dificultad, maximo, minimo, puntos, tiempo, numeroPantallas, numeroPantallaActual ;
    int aciertos; //lleva el numero de aciertos para mostrar al finalizar el juego

    Typeface font;

    /*
    * tipoOperacion
    *   0 --> mayo/menor
    *   1 --> suma
    *   2 --> resta
    * */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), ConfiguracionJuegoActivity.class);
            startActivity(intent);
            contador.cancel();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);

        getSupportActionBar().setTitle(getString(R.string.juego_titulo));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName), MODE_PRIVATE);
        dificultad = sharedPref.getInt(getString(R.string.config_dificultad_juego), 1);
        minimo = sharedPref.getInt(getString(R.string.config_minimo_juego), 1);
        maximo = sharedPref.getInt(getString(R.string.config_maximo_juego), 20);
        numeroPantallas = sharedPref.getInt(getString(R.string.config_cantidad_pantallas), 10);
        rnd = new Random();

        font = Typeface.createFromAsset(getAssets(), "Levator.ttf");

        txtPrimerOp = (TextView) findViewById(R.id.txtPrimerOperando);
        txtSegundoOp = (TextView) findViewById(R.id.txtSegundoOperando);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        txtSigno = (TextView) findViewById(R.id.txtSigno);
        txtIgual = (TextView) findViewById(R.id.txtIgual);
        txtPuntaje = (TextView) findViewById(R.id.txtPuntaje);
        txtTiempo = (TextView) findViewById(R.id.txtTiempo) ;
        txtPantalla = (TextView) findViewById(R.id.txtPantalla) ;
        opciones = new ArrayList<>();
        posiciones = new ArrayList<>();

        txtPrimerOp.setTypeface(font);
        txtSegundoOp.setTypeface(font);
        txtResultado.setTypeface(font);
        txtPuntaje.setTypeface(font);
        txtTiempo.setTypeface(font);
        txtPantalla.setTypeface(font);

        primeroCompleto = false;
        segundoCompleto = false;
        resultadoCompleto = false;

        grid = (GridLayout) findViewById(R.id.grid);

        //obtiene las dimensiones de la pantalla para ajustar el tamaño del texto y del grid
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if( size.x >= 820){//evalua el tamaño (ancho) de pantalla
            textSize = 40;
            height =  width = 100;
            grid.setRowCount(4);
            grid.setColumnCount(15);
        }else{
            textSize = 25;
            height = width = 50;
            grid.setRowCount(3);
            grid.setColumnCount(14);
        }

        this.seleccionarOperacion();
        this.completarGrid();
        Animation deslizar;
        deslizar = AnimationUtils.loadAnimation(this, R.anim.deslizar_in);
        grid.startAnimation(deslizar);

        switch (dificultad){//lleva el tiempo de cada pantalla.
            case DIFICULTAD_FACIL:
                tiempo = 37000;
                break;
            case DIFICULTAD_MEDIA:
                tiempo = 52000;
                break;
            case DIFICULTAD_DIFICIL:
                tiempo = 37000;
                break;
        }

         contador = new CountDownTimer(tiempo, 1000) {

            public void onTick(long millisUntilFinished) {
                txtTiempo.setText(String.valueOf(millisUntilFinished/1000));
                if(completo)
                    cancel();
            }

            public void onFinish() {
                pantallaSiguiente(false);
            }
        }.start();


        try { //recupera los puntos acumulados de la pantalla anterior, los aciertos y el numero de pantalla actual
            puntos = getIntent().getExtras().getInt("puntos");
            aciertos = getIntent().getExtras().getInt("aciertos");
            numeroPantallaActual = getIntent().getExtras().getInt("numeroPantallaActual");
            txtPuntaje.setText(String.valueOf(puntos));
        }
        catch(NullPointerException ex){
            aciertos=0;
            puntos = 0;
            numeroPantallaActual = 1;
            txtPuntaje.setText("0" );
        }

        txtPantalla.setText(numeroPantallaActual + "/" + numeroPantallas);

        txtPrimerOp.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch(event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        return true;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        v.setBackgroundColor(Color.LTGRAY);
                        return true;

                    case DragEvent.ACTION_DRAG_LOCATION:
                       v.setVisibility(View.VISIBLE);
                        return true;

                    case DragEvent.ACTION_DRAG_EXITED:
                        txtPrimerOp.setVisibility(View.VISIBLE);
                        v.setBackgroundColor(Color.TRANSPARENT);
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        return true;

                    case DragEvent.ACTION_DROP:
                        txtPrimerOp.setText(txtSeleccionado.getText());
                        txtPrimerOp.setTextColor(Color.WHITE);
                        txtPrimerOp.setOnDragListener(null);
                        txtSeleccionado.setOnTouchListener(null);
                        txtSeleccionado.setBackgroundResource(R.drawable.nube_gris);
                        primeroCompleto = true;
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        comprobar();
                        return true;

                    case DragEvent.ACTION_DRAG_ENDED:
                        txtSeleccionado.setVisibility(View.VISIBLE);
                        return true;

                    default:
                        break;
                }
                return false;
            }
        });

        txtSegundoOp.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch(event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        return true;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        v.setBackgroundColor(Color.LTGRAY);
                        return true;

                    case DragEvent.ACTION_DRAG_LOCATION:
                        v.setVisibility(View.VISIBLE);
                        return true;

                    case DragEvent.ACTION_DRAG_EXITED:
                        txtSegundoOp.setVisibility(View.VISIBLE);
                        v.setBackgroundColor(Color.TRANSPARENT);
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        return true;

                    case DragEvent.ACTION_DROP:
                        txtSegundoOp.setText(txtSeleccionado.getText());
                        txtSegundoOp.setTextColor(Color.WHITE);
                        txtSegundoOp.setOnDragListener(null);
                        txtSeleccionado.setOnTouchListener(null);
                        txtSeleccionado.setBackgroundResource(R.drawable.nube_gris);
                        segundoCompleto=true;
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        comprobar();
                        return true;

                    case DragEvent.ACTION_DRAG_ENDED:
                        txtSeleccionado.setVisibility(View.VISIBLE);
                        return true;

                    default:
                        break;
                }
                return false;
            }
        });


        txtResultado.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch(event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        return true;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        v.setBackgroundColor(Color.LTGRAY);
                        txtResultado.setVisibility(View.INVISIBLE);
                        return true;

                    case DragEvent.ACTION_DRAG_LOCATION:
                        v.setVisibility(View.VISIBLE);
                        return true;

                    case DragEvent.ACTION_DRAG_EXITED:
                        txtResultado.setVisibility(View.VISIBLE);
                        v.setBackgroundColor(Color.TRANSPARENT);
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        return true;

                    case DragEvent.ACTION_DROP:
                        txtResultado.setText(txtSeleccionado.getText());
                        txtResultado.setTextColor(Color.WHITE);
                        txtResultado.setOnDragListener(null);
                        resultadoCompleto=true;
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        txtSeleccionado.setOnTouchListener(null);
                        txtSeleccionado.setBackgroundResource(R.drawable.nube_gris);
                        comprobar();
                        return true;

                    case DragEvent.ACTION_DRAG_ENDED:
                        txtSeleccionado.setVisibility(View.VISIBLE);
                        return true;

                    default:
                        break;
                }
                return false;
            }
        });


    }//onCreate

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        ClipData data = ClipData.newPlainText("", "");
        txtPrimerOp.startDrag(data, new View.DragShadowBuilder(v), null, 0);
        txtSegundoOp.startDrag(data, new View.DragShadowBuilder(v), null, 0);
        txtSigno.startDrag(data, new View.DragShadowBuilder(v), null, 0);
        txtResultado.startDrag(data, new View.DragShadowBuilder(v), null, 0);
        txtSeleccionado = (TextView) v;
        v.setVisibility(View.INVISIBLE);
        return true;
    }

    public void completarGrid(){//carga el grid con las opciones y con textView vacios
        this.generarOpciones(); // antes de cargar las opciones las genera
        grid.removeAllViews();
        int filas = grid.getRowCount();
        int columnas = grid.getColumnCount();
        Point posicion = new Point();
        for(int i=0; i<filas; i++){
            for(int j=0; j<columnas; j++){
                TextView tv = new TextView(this);
                posicion.set(i,j);
                if(posiciones.contains(posicion)){ //si en esa posicion hay una opcion la cargo, si no cargo un textView vacio
                    tv.setText(String.valueOf(opciones.get(0)));
                    tv.setTextSize(textSize);
                    tv.setTypeface(font);
                    tv.setOnTouchListener(this);
                    tv.setBackgroundResource(R.drawable.nube);
                    tv.setGravity(Gravity.CENTER);
                    opciones.remove(0);
                }else{
                    tv.setVisibility(View.INVISIBLE);
                }
                tv.setHeight(height);
                tv.setWidth(width);
                GridLayout.LayoutParams params = new GridLayout.LayoutParams(GridLayout.spec(i), GridLayout.spec(j));
                tv.setLayoutParams(params);
                grid.addView(tv);
            }

        }

    }


    public void generarOpciones(){
        int primero, segundo;
        switch (tipoOperacion){
            case OP_MAYOR_MENOR:
                String operador= this.elegirOperador(); //retorna un operador mayor, menor o igual random
                resultadoCompleto = true; // el Txt resultado no se va a tener en cuenta, se considera correcto
                if(operador.equals("=")){
                    primero = (rnd.nextInt(maximo-minimo) + 1)+ minimo;
                    opciones.add(primero);
                    opciones.add(primero);
                }
                txtSigno.setText(operador);
                txtSigno.setTextColor(Color.WHITE);
                txtIgual.setVisibility(View.GONE);
                txtResultado.setVisibility(View.GONE);
                break;
            case OP_SUMA:
                primero = (rnd.nextInt(maximo-minimo) + 1)+ minimo;
                segundo = (rnd.nextInt(maximo-primero) + 1)+ primero;
                opciones.add(primero);
                opciones.add(segundo);
                opciones.add(primero+segundo);
                txtIgual.setTextColor(Color.WHITE);
                txtSigno.setText("+");
                txtSigno.setTextColor(Color.WHITE);
                break;
            case OP_RESTA:
                primero = (rnd.nextInt(maximo-minimo) + 1)+ minimo;
                segundo = (rnd.nextInt(maximo-minimo) + 1)+ minimo;
                while (primero == segundo){
                    segundo = (rnd.nextInt(maximo-minimo) + 1)+ minimo;
                }
                if(primero < segundo){
                    int aux = primero;
                    primero = segundo;
                    segundo= aux;
                }
                opciones.add(primero);
                opciones.add(segundo);
                opciones.add(primero-segundo);
                txtIgual.setTextColor(Color.WHITE);
                txtSigno.setText("-");
                txtSigno.setTextColor(Color.WHITE);
                break;
        }
        int cantidad = opciones.size(); // para saber cuantas opciones extra agregar
        for(int i=0; i< (10 - cantidad); i++){
            opciones.add((rnd.nextInt(maximo-minimo) + 1)+ minimo);
        }
        Collections.shuffle(opciones);//desordena el array
        this.generarPosiciones(); //una vez generada las opciones, genera las posiciones en donde se van a mostrar
    }

    public void generarPosiciones(){
        posiciones.clear();
        int filas = grid.getRowCount();
        int columnas = grid.getColumnCount();
        Random rnd = new Random();
        for(int opcion : opciones){ //calculo una posicion para cada opcion
            Point posicion = new Point();
            posicion.set(rnd.nextInt(filas), rnd.nextInt(columnas));
            while(!verificaPosicion(posicion)){
                posicion.set(rnd.nextInt(filas), rnd.nextInt(columnas));
            }
            posiciones.add(posicion);
        }
    }

    public boolean verificaPosicion(Point posicion){ //verifica si la posicion [fila-columna] esta disponible
        for(Point pos : posiciones){
            if((pos.x == posicion.x) && (pos.y == posicion.y)){
                return false;
            }
        }
        return true;
    }

    public String elegirOperador(){//retorna un operador >, < o =
        ArrayList<String> operadores = new ArrayList<>();
        operadores.add(">");
        operadores.add("<");
        operadores.add("=");
        rnd = new Random();
        return operadores.get(rnd.nextInt(3));
    }

    public void seleccionarOperacion(){
        switch (dificultad){
            case DIFICULTAD_FACIL:
                tipoOperacion = OP_MAYOR_MENOR;
                break;
            case DIFICULTAD_MEDIA:
                tipoOperacion = rnd.nextInt(DIFICULTAD_MEDIA);//random entre 0 y 1
                break;
            case DIFICULTAD_DIFICIL:
                tipoOperacion = rnd.nextInt(DIFICULTAD_MEDIA) + 1;//random entre 1 y 2
                break;
        }
    }

    public void comprobar(){ //valida que esten los tres campos completos y el resultado correcto
        if(primeroCompleto && segundoCompleto && resultadoCompleto){
            completo = true;
            boolean resultado = validarResultado();
            if(resultado){
                puntajes();
                aciertos++;
            }
            pantallaSiguiente(resultado);
        }else{
            MediaPlayer mp = MediaPlayer.create(this, R.raw.audio_drag_end);
            mp.start();
        }
    }

    public Boolean validarResultado(){

        int primero = Integer.parseInt(txtPrimerOp.getText().toString());
        int segundo = Integer.parseInt(txtSegundoOp.getText().toString());
        String signo = txtSigno.getText().toString();

        switch (tipoOperacion){
            case OP_MAYOR_MENOR:
                if((primero > segundo) && signo.equals(">")){
                    return true;
                }else{
                    if((primero < segundo) && signo.equals("<")){
                        return true;
                    }else{
                        if((primero == segundo) && signo.equals("=")){
                            return true;
                        }else{
                            return false;
                        }
                    }
                }
            case OP_SUMA:
                if((primero + segundo) == Integer.parseInt(txtResultado.getText().toString())){
                    return true;
                }else{
                    return false;
                }
            case OP_RESTA:
                if((primero - segundo) == Integer.parseInt(txtResultado.getText().toString())){
                    return true;
                }else{
                    return false;
                }
        }
        return false;
    }

    public void pantallaSiguiente(Boolean r) { //muestra el resultado y cambia a la siguiente pantalla
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        if (r == true) {
            final View view = inflater.inflate(R.layout.dialog_exito, null);
            builder.setView(view);
            MediaPlayer mp = MediaPlayer.create(this, R.raw.bien);
            mp.start();
        }else{
            final View view = inflater.inflate(R.layout.dialog_error, null);
            builder.setView(view);
            MediaPlayer mp = MediaPlayer.create(this, R.raw.error);
            mp.start();
        }
        final AlertDialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show(); //muestra si acerto o erro

        contador.cancel(); // cancela el contador del tiempo

        // espera tres segundos para cambiar de activity
        final Thread thread=  new Thread(){
            @Override
            public void run(){
                try {
                    synchronized(this){
                        wait(3000);
                        dialog.dismiss();

                        //termina el juego y muestra la informacion
                        if(numeroPantallas == numeroPantallaActual){
                            int puntajeMaximo = getPuntajeMaximo(); //puntaje maximo de las partidas anteriores
                            if(puntos > puntajeMaximo){
                                setPuntajeMaximo(puntos);
                            }
                            Intent intent = new Intent(getApplicationContext(), FinJuegoActivity.class);
                            intent.putExtra("puntos", puntos);
                            intent.putExtra("puntajeMaximo", puntajeMaximo);
                            intent.putExtra("aciertos", aciertos);
                            intent.putExtra("pantallas", numeroPantallas);
                            intent.putExtra("dificultad", dificultad);
                            startActivity(intent);
                            finish();
                        }else {
                            Intent intent = getIntent();
                            intent.putExtra("puntos", puntos);
                            intent.putExtra("aciertos", aciertos);
                            intent.putExtra("numeroPantallaActual", numeroPantallaActual+1);
                            startActivity(intent);
                            finish();
                        }
                    }
                }
                catch(InterruptedException ex){}
            }
        };
        thread.start();
    }

    public int getPuntajeMaximo(){ //lee de un sharedPreferences el puntaje maximo de esa dificultad
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName), MODE_PRIVATE);
        switch (dificultad){
            case DIFICULTAD_FACIL:
                return sharedPref.getInt(getString(R.string.config_puntaje_facil), 0);
            case DIFICULTAD_MEDIA:
                return sharedPref.getInt(getString(R.string.config_puntaje_medio), 0);
            case DIFICULTAD_DIFICIL:
                return sharedPref.getInt(getString(R.string.config_puntaje_dificil), 0);
            default:
                return 0;
        }

    }

    public void setPuntajeMaximo(int puntaje){ //guarda en un sharedPreferences el puntaje maximo de esa dificultad
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName),MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        switch (dificultad){
            case DIFICULTAD_FACIL:
                editor.putInt(getString(R.string.config_puntaje_facil), Integer.valueOf(puntaje));
            case DIFICULTAD_MEDIA:
                editor.putInt(getString(R.string.config_puntaje_medio), Integer.valueOf(puntaje));
            case DIFICULTAD_DIFICIL:
                editor.putInt(getString(R.string.config_puntaje_dificil), Integer.valueOf(puntaje));

        }
        editor.commit();
    }
    public void puntajes(){
        switch (dificultad) {
            case DIFICULTAD_FACIL:
                puntos += 3;
                break;
            case DIFICULTAD_MEDIA:
                puntos += 5;
                break;
            case DIFICULTAD_DIFICIL:
                puntos += 10;
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        contador.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}