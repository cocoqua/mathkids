package ar.edu.unnoba.ppc2016.juego;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.TextView;
import java.util.ArrayList;
import ar.edu.unnoba.ppc2016.R;

public class ConfiguracionJuegoActivity extends AppCompatActivity {

    private static final int DIFICULTAD_FACIL = 1;
    private static final int DIFICULTAD_MEDIA = 2;
    private static final int DIFICULTAD_DIFICIL = 3;
    private static final String MIN = "valor_minimo";
    private static final String MAX = "valor_maximo";


    RadioButton dificultadFacil;
    RadioButton dificultadMedio;
    RadioButton dificultadDificil;
    Button btnGuardar;
    TextView error, tvValorMaximo, tvValorMinimo, textValorMaximo, textValorMinimo;
    EditText etNumeroPantallas;
    int dificultad, numeroPantallas;
    ArrayList<String> valores; //valores para mostrar en los pikers
    String minimo, maximo; //maximo y minimo guardan la seleccion actual de los pickers

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion_juego);
        getSupportActionBar().setTitle(getString(R.string.configuracion));

        Typeface font = Typeface.createFromAsset(getAssets(), "Levator.ttf");

        error = (TextView) findViewById(R.id.error);
        tvValorMaximo = (TextView) findViewById(R.id.valorMaximo);//contiene el valor
        tvValorMinimo = (TextView) findViewById(R.id.valorMinimo);//contiene el valor
        textValorMaximo = (TextView) findViewById(R.id.textValorMaximo);//contiene el texto
        textValorMinimo = (TextView) findViewById(R.id.textValorMinimo);//contiene el texto
        dificultadFacil = (RadioButton) findViewById(R.id.dificultadFacil);
        dificultadMedio = (RadioButton) findViewById(R.id.dificultadMedio);
        dificultadDificil = (RadioButton) findViewById(R.id.dificultadDificil);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        etNumeroPantallas = (EditText) findViewById(R.id.numeroPantallas);

        tvValorMaximo.setTypeface(font);
        tvValorMinimo.setTypeface(font);
        etNumeroPantallas.setTypeface(font);

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName), MODE_PRIVATE);
        minimo = String.valueOf(sharedPref.getInt(getString(R.string.config_minimo_juego), 1));
        maximo = String.valueOf(sharedPref.getInt(getString(R.string.config_maximo_juego), 20));
        dificultad = sharedPref.getInt(getString(R.string.config_dificultad_juego), 1);
        numeroPantallas = sharedPref.getInt(getString(R.string.config_cantidad_pantallas), 10);

        etNumeroPantallas.setText(String.valueOf(numeroPantallas));
        tvValorMinimo.setText(minimo);
        tvValorMaximo.setText(maximo);

        valores = new ArrayList<>();
        valores.add("1");
        valores.add("10");
        valores.add("20");
        valores.add("30");
        valores.add("40");
        valores.add("50");
        valores.add("60");
        valores.add("70");
        valores.add("80");
        valores.add("90");
        valores.add("100");

        final String[] val = valores.toArray(new String[valores.size()]);

        switch (dificultad){
            case DIFICULTAD_FACIL:
                dificultadFacil.setChecked(true);
                dificultadMedio.setChecked(false);
                dificultadDificil.setChecked(false);
                break;

            case DIFICULTAD_MEDIA:
                dificultadFacil.setChecked(false);
                dificultadMedio.setChecked(true);
                dificultadDificil.setChecked(false);
                break;

            case DIFICULTAD_DIFICIL:
                dificultadFacil.setChecked(false);
                dificultadMedio.setChecked(false);
                dificultadDificil.setChecked(true);
                break;

            default:
                break;
        }

        tvValorMinimo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarPiker(val, minimo, MIN);
            }
        });

        textValorMinimo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarPiker(val, minimo, MIN);
            }
        });

        tvValorMaximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarPiker(val, maximo, MAX);
            }
        });

        textValorMaximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarPiker(val, maximo, MAX);
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verificarDificultad();
                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName),MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(getString(R.string.config_maximo_juego), Integer.valueOf(maximo));
                editor.putInt(getString(R.string.config_minimo_juego),Integer.valueOf(minimo));
                editor.putInt(getString(R.string.config_dificultad_juego), dificultad);
                editor.putInt(getString(R.string.config_cantidad_pantallas), validarNumeroPantallas());
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), JuegoActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }//onCreate

    public int validarNumeroPantallas(){
        String pantallas = etNumeroPantallas.getText().toString();
            if (!pantallas.equals("") && !pantallas.equals("0"))
                return Integer.valueOf(pantallas);
        return numeroPantallas; // si se asigna nulo o cero, retorna el mismo valor que ya tenia
    }

    public void verificarDificultad(){
        if(dificultadFacil.isChecked()){
            dificultad = DIFICULTAD_FACIL;
        }else{
            if(dificultadMedio.isChecked()){
                dificultad = DIFICULTAD_MEDIA;
            }else{
                dificultad = DIFICULTAD_DIFICIL;
            }
        }
    }


    //valida que el rango numerico sea correcto minimo < maximo
    //tipo indica si se trata del valor maximo o el minimo
    public void validar(int indiceSeleccionado, String tipo) {
        if(tipo.equals(MIN)){ //asigna la variable correspondiente con el valor del rango seleccionado y setea el textView
            minimo = valores.get(indiceSeleccionado);
            tvValorMinimo.setText(minimo);
        }else{
            maximo = valores.get(indiceSeleccionado);
            tvValorMaximo.setText(maximo);
        }


        if (Integer.parseInt(minimo) < Integer.parseInt(maximo)) {
            error.setVisibility(View.INVISIBLE);
            btnGuardar.setEnabled(true);
            btnGuardar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            btnGuardar.setTextColor(Color.WHITE);
        } else {
            error.setVisibility(View.VISIBLE);
            btnGuardar.setEnabled(false);
            btnGuardar.setBackgroundColor(Color.GRAY);
            btnGuardar.setTextColor(Color.DKGRAY);
        }

    }

    public void mostrarPiker(String[] val, String valorActual, String tipo){
        //tipo indica si se trata del valor maximo o el minimo
        final NumberPicker np = new NumberPicker(this);
        final String type = tipo;
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np.setMinValue(0);
        np.setMaxValue(valores.size() - 1);
        np.setDisplayedValues(val);
        np.setValue(valores.indexOf(valorActual));


        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        if(tipo.equals(MIN)){
            alert.setTitle("Seleccione el valor mínimo del intervalo.");
        }else{
            alert.setTitle("Seleccione el valor máximo del intervalo.");
        }

        alert.setView(np);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                int valor = np.getValue();
                validar(valor, type);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });

        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
