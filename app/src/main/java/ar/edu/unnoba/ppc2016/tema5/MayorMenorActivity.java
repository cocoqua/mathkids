package ar.edu.unnoba.ppc2016.tema5;

import android.content.ClipData;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import ar.edu.unnoba.ppc2016.R;

import java.util.Random;

public class MayorMenorActivity extends AppCompatActivity implements View.OnTouchListener, View.OnDragListener{

    private TextView textViewPrimerValor;
    private TextView textViewSegundoValor;
    private TextView txtIntervalo; // se informa el intervalo numerico que esta trabajando
    private ImageView imageViewSigno;
    private ImageView imageViewSimboloMenor;
    private ImageView imageViewSimboloIgual;
    private ImageView imageViewSimboloMayor;
    private ImageView imagenSeleccionada;
    private Random rnd;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), ConfiguracionMayorMenorActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mayor_menor);

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName), MODE_PRIVATE);
        Integer minimo = sharedPref.getInt(getString(R.string.configMinimo),1);
        Integer maximo = sharedPref.getInt(getString(R.string.configMaximo), 20);

        getSupportActionBar().setTitle(getString(R.string.mayor_menor_titulo));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textViewPrimerValor = (TextView) findViewById(R.id.primerValor);
        textViewSegundoValor = (TextView) findViewById(R.id.segundoValor);
        txtIntervalo = (TextView) findViewById(R.id.txtIntervalo);


        Typeface font = Typeface.createFromAsset(getAssets(), "Levator.ttf");
        textViewPrimerValor.setTypeface(font);
        textViewSegundoValor.setTypeface(font);

        imageViewSigno = (ImageView) findViewById(R.id.signo);
        imageViewSimboloMenor = (ImageView) findViewById(R.id.simboloMenor);
        imageViewSimboloIgual = (ImageView) findViewById(R.id.simboloIgual);
        imageViewSimboloMayor = (ImageView) findViewById(R.id.simboloMayor);
        imageViewSimboloIgual.setTag("=");
        imageViewSimboloMayor.setTag(">");
        imageViewSimboloMenor.setTag("<");

        imageViewSimboloIgual.getDrawable().clearColorFilter();
        imageViewSimboloMenor.getDrawable().clearColorFilter();
        imageViewSimboloMayor.getDrawable().clearColorFilter();

        imageViewSimboloMayor.setOnTouchListener(this);
        imageViewSimboloIgual.setOnTouchListener(this);
        imageViewSimboloMenor.setOnTouchListener(this);

        imageViewSigno.setOnDragListener(this);



        rnd = new Random();
        textViewPrimerValor.setText(String.valueOf((rnd.nextInt(maximo-minimo) + 1)+ minimo) );
        textViewSegundoValor.setText(String.valueOf((rnd.nextInt(maximo-minimo) + 1) + minimo));
        txtIntervalo.setText("RANGO: " + minimo.toString() + " - " + maximo.toString());
    }

    public void evaluar(){
        Integer a = Integer.valueOf(textViewPrimerValor.getText().toString());
        Integer b = Integer.valueOf(textViewSegundoValor.getText().toString());
        if(a < b){
            if(imagenSeleccionada.getTag() == "<"){
                imageViewSigno.setImageResource(R.drawable.menor);
                correcto();
            }else {
                incorrecto();
            }
        }else{
            if(a == b){
                if(imagenSeleccionada.getTag() == "="){
                    imageViewSigno.setImageResource(R.drawable.igual);
                    correcto();
                }else {
                    incorrecto();
                }
            }else{
                if(imagenSeleccionada.getTag() == ">"){
                    imageViewSigno.setImageResource(R.drawable.mayor);
                    correcto();
                }else {
                    incorrecto();
                }
            }
        }
    }

    public void correcto(){
        MediaPlayer mp = MediaPlayer.create(this, R.raw.bien);
        mp.start();
        imagenSeleccionada.getDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY );
        Thread thread=  new Thread(){
            @Override
            public void run(){
                try {
                    synchronized(this){

                        wait(3000);
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);

                    }
                }
                catch(InterruptedException ex){}
            }
        };

        thread.start();
    }

    public void incorrecto(){
        MediaPlayer mp = MediaPlayer.create(this, R.raw.error);
        mp.start();
        imageViewSigno.setImageResource(R.drawable.linea_divisora_shape);
        imagenSeleccionada.getDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY );
        imagenSeleccionada.setOnTouchListener(null);

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        ClipData data = ClipData.newPlainText("", "");
        imageViewSigno.startDrag(data, new View.DragShadowBuilder(v), null, 0);
        v.setVisibility(View.INVISIBLE);
        imagenSeleccionada = (ImageView)v;
        return true;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        switch(event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                return true;

            case DragEvent.ACTION_DRAG_ENTERED:
                v.setBackgroundColor(Color.LTGRAY);
                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
                v.setVisibility(View.VISIBLE);

                return true;

            case DragEvent.ACTION_DRAG_EXITED:
                v.setBackgroundColor(Color.TRANSPARENT);
                return true;

            case DragEvent.ACTION_DROP:
                v.setBackgroundColor(Color.TRANSPARENT);
                evaluar();
                return true;

            case DragEvent.ACTION_DRAG_ENDED:
                imagenSeleccionada.setVisibility(View.VISIBLE);
                return true;

            default:
                break;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}

