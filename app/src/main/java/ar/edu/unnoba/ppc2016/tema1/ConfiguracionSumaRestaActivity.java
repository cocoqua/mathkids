package ar.edu.unnoba.ppc2016.tema1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import ar.edu.unnoba.ppc2016.R;

public class ConfiguracionSumaRestaActivity extends AppCompatActivity {


    private RadioButton rbSuma;
    private RadioButton rbSumaResta;
    private Button btnGuardar;
    private boolean soloSuma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion_suma_resta);

        getSupportActionBar().setTitle(getString(R.string.configuracion));

        rbSuma = (RadioButton) findViewById(R.id.suma);
        rbSumaResta = (RadioButton) findViewById(R.id.sumaResta);
        btnGuardar =(Button) findViewById(R.id.btnGuardar);

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName),Context.MODE_PRIVATE);
        soloSuma = sharedPref.getBoolean(getString(R.string.configSumaResta), false);

        if (soloSuma){
            rbSuma.setChecked(true);
            rbSumaResta.setChecked(false);
        }else{
            rbSumaResta.setChecked(true);
            rbSuma.setChecked(false);
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName),Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.configSumaResta),soloSuma);
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), SumaRestaActivity.class);
                startActivity(intent);
                finish();
            }
        });
        rbSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soloSuma=true;
            }
        });

        rbSumaResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soloSuma=false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
