package ar.edu.unnoba.ppc2016.tema1;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import ar.edu.unnoba.ppc2016.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SumaRestaActivity extends AppCompatActivity implements View.OnTouchListener {

    private ImageView imagePrimerOperando;
    private ImageView imagePrimerDado;
    private ImageView imageOperador;
    private ImageView imageSegundoOperando;
    private ImageView imageSegundoDado;
    private ImageView imageResultado;
    private ImageView imagePrimerNumero;
    private ImageView imageSegundoNumero;
    private ImageView imageTercerNumero;
    private ImageView imageCuartoNumero;
    private ImageView imageQuintoNumero;
    private TextView txtIntervalo;
    private ImageView imagenSeleccionada;//imagen que ejecuto el onDrag
    private Map dadosVal; //map con los valores de los dados como key
    private Map numerosVal;//map con los valores de los numeros como key
    private Random rnd;
    private ArrayList<Integer> opciones; // todas las opciones para completar los campos
    private boolean soloSuma; //configuracion para hacer solo sumas o sumas y restas
    private boolean esSuma = true; //la operacion actual
    private int valorDado1;
    private int valorDado2;
    private boolean opcionUnoCorrecta;//para obligar a cargar los operandos en orden
    private boolean opcionDosCorrecta;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), ConfiguracionSumaRestaActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suma_resta);
        getSupportActionBar().setTitle(getString(R.string.suma_resta_titulo));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName),Context.MODE_PRIVATE);
        soloSuma = sharedPref.getBoolean(getString(R.string.configSumaResta), false);
        opcionUnoCorrecta = false;
        opcionDosCorrecta = false;
        rnd = new Random();
        imagePrimerOperando = (ImageView) findViewById(R.id.primerIncognita);
        imagePrimerDado = (ImageView) findViewById(R.id.primerDado);
        imageOperador = (ImageView) findViewById(R.id.operador);
        imageSegundoOperando = (ImageView) findViewById(R.id.segundaIncognita);
        imageSegundoDado = (ImageView) findViewById(R.id.segundoDado);
        imageResultado = (ImageView) findViewById(R.id.resultado);

        imagePrimerNumero = (ImageView) findViewById(R.id.primerNumero);
        imageSegundoNumero = (ImageView) findViewById(R.id.segundoNumero);
        imageTercerNumero = (ImageView) findViewById(R.id.tercerNumero);
        imageCuartoNumero = (ImageView) findViewById(R.id.cuartoNumero);
        imageQuintoNumero = (ImageView) findViewById(R.id.quintoNumero);

        imagePrimerNumero.setOnTouchListener(this);
        imageSegundoNumero.setOnTouchListener(this);
        imageTercerNumero.setOnTouchListener(this);
        imageCuartoNumero.setOnTouchListener(this);
        imageQuintoNumero.setOnTouchListener(this);

        txtIntervalo = (TextView) findViewById(R.id.txtIntervalo);

        dadosVal = new HashMap<Integer, Integer>();
        numerosVal = new HashMap<Integer, Integer>();
        opciones = new ArrayList<>();
        cargarImagenes();

        if(soloSuma) {
            esSuma = true;
            txtIntervalo.setText(R.string.op_solo_suma);
        }else{
            esSuma= rnd.nextBoolean();
            txtIntervalo.setText(R.string.op_suma_resta);
        }

        //carga las dos imagenes de los dados
        valorDado1 = rnd.nextInt(dadosVal.size())+1;
        valorDado2 = rnd.nextInt(dadosVal.size()) + 1;
        if(esSuma){
            imageOperador.setImageResource(R.drawable.suma);
            opciones.add(valorDado1+valorDado2);//cargo el resultado a las opciones
        }else{
            validarMayorMenor();
            opciones.add(valorDado1-valorDado2); //cargo el resultado a las opciones
            imageOperador.setImageResource(R.drawable.resta);
        }
        imagePrimerDado.setImageResource((Integer)dadosVal.get(valorDado1));
        imageSegundoDado.setImageResource((Integer)dadosVal.get(valorDado2));
        //cargo los dos valores a las opciones
        opciones.add(valorDado1);
        opciones.add(valorDado2);
        completarOpciones(); //completa con las dos opciones faltantes

        //muestra las imagenes en la vista
        imagePrimerNumero.setImageResource((Integer)numerosVal.get(opciones.get(0)));
        imagePrimerNumero.setTag(opciones.get(0));
        imageSegundoNumero.setImageResource((Integer)numerosVal.get(opciones.get(1)));
        imageSegundoNumero.setTag(opciones.get(1));
        imageTercerNumero.setImageResource((Integer)numerosVal.get(opciones.get(2)));
        imageTercerNumero.setTag(opciones.get(2));
        imageCuartoNumero.setImageResource((Integer)numerosVal.get(opciones.get(3)));
        imageCuartoNumero.setTag(opciones.get(3));
        imageQuintoNumero.setImageResource((Integer)numerosVal.get(opciones.get(4)));
        imageQuintoNumero.setTag(opciones.get(4));

        imagePrimerOperando.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch(event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        return true;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        v.setBackgroundColor(Color.LTGRAY);
                        return true;

                    case DragEvent.ACTION_DRAG_LOCATION:
                        v.setVisibility(View.VISIBLE);
                        return true;

                    case DragEvent.ACTION_DRAG_EXITED:
                        v.setBackgroundColor(Color.TRANSPARENT);
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        return true;

                    case DragEvent.ACTION_DROP:
                        int valor = (Integer)imagenSeleccionada.getTag();
                        if(verificarPrimero(valor)){
                            imagePrimerOperando.setImageResource((Integer)numerosVal.get(valor));
                            imagePrimerOperando.setVisibility(View.VISIBLE);
                            imagenSeleccionada.setBackgroundResource(R.drawable.linea_correcto_shape);
                            imagenSeleccionada.setOnTouchListener(null);
                            imagePrimerOperando.setOnDragListener(null);
                        }
                        v.setBackgroundColor(Color.TRANSPARENT);
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        return true;

                    case DragEvent.ACTION_DRAG_ENDED:
                        imagenSeleccionada.setVisibility(View.VISIBLE);
                        return true;

                    default:
                        break;
                }
                return false;
            }
        });

        imageSegundoOperando.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch(event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        return true;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        v.setBackgroundColor(Color.LTGRAY);
                        return true;

                    case DragEvent.ACTION_DRAG_LOCATION:
                        v.setVisibility(View.VISIBLE);
                        return true;

                    case DragEvent.ACTION_DRAG_EXITED:
                        v.setBackgroundColor(Color.TRANSPARENT);
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        return true;

                    case DragEvent.ACTION_DROP:
                        int valor = (Integer)imagenSeleccionada.getTag();
                        if(verificarSegundo(valor)) {
                            imageSegundoOperando.setImageResource((Integer) numerosVal.get(valor));
                            imagenSeleccionada.setBackgroundResource(R.drawable.linea_correcto_shape);
                            imagenSeleccionada.setOnTouchListener(null);
                            imageSegundoOperando.setOnDragListener(null);
                        }
                        v.setBackgroundColor(Color.TRANSPARENT);
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        return true;

                    case DragEvent.ACTION_DRAG_ENDED:
                        imagenSeleccionada.setVisibility(View.VISIBLE);
                        return true;

                    default:
                        break;
                }
                return false;
            }
        });

        imageResultado.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch(event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        return true;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        v.setBackgroundColor(Color.LTGRAY);
                        return true;

                    case DragEvent.ACTION_DRAG_LOCATION:
                        v.setVisibility(View.VISIBLE);
                        return true;

                    case DragEvent.ACTION_DRAG_EXITED:
                        v.setBackgroundColor(Color.TRANSPARENT);
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        return true;

                    case DragEvent.ACTION_DROP:
                        v.setBackgroundColor(Color.TRANSPARENT);
                        int valor = (Integer)imagenSeleccionada.getTag();
                        if(verificarResultado(valor)){
                            Thread thread=  new Thread(){
                                @Override
                                public void run(){
                                    try {
                                        synchronized(this){
                                            wait(3000);
                                            Intent intent = getIntent();
                                            finish();
                                            startActivity(intent);
                                        }
                                    }
                                    catch(InterruptedException ex){}
                                }
                            };
                            imageResultado.setImageResource((Integer)numerosVal.get(valor));
                            imagenSeleccionada.setBackgroundResource(R.drawable.linea_correcto_shape);
                            imagenSeleccionada.setOnTouchListener(null);
                            //imageResultado.setOnDragListener(null);
                            thread.start();
                        }
                        v.setBackgroundColor(Color.TRANSPARENT);
                        v.setBackgroundResource(R.drawable.linea_divisora_shape);
                        return true;

                    case DragEvent.ACTION_DRAG_ENDED:
                        imagenSeleccionada.setVisibility(View.VISIBLE);
                        return true;

                    default:
                        break;
                }
                return false;
            }
        });
    }

    //verifica que la opcion elegida sea correcta
    public boolean verificarPrimero(int valor){
        if(valor == valorDado1){
            MediaPlayer mp = MediaPlayer.create(this, R.raw.bien);
            mp.start();
            opcionUnoCorrecta=true;
            return true;
        }
        MediaPlayer mp = MediaPlayer.create(this, R.raw.error);
        mp.start();
        return false;
    }

    public boolean verificarSegundo(int valor){
        if(opcionUnoCorrecta){
            if (valor == valorDado2) {
                MediaPlayer mp = MediaPlayer.create(this, R.raw.bien);
                mp.start();
                opcionDosCorrecta = true;
                return true;
            }
        }else{
            imagePrimerOperando.setBackgroundResource(R.drawable.recuadro_rojo_shape);

        }
        MediaPlayer mp = MediaPlayer.create(this, R.raw.error);
        mp.start();
        return false;
    }

    public boolean verificarResultado(int valor){
        int resultado;
        if(esSuma){
            resultado = valorDado1 + valorDado2;
        }else{
            resultado = valorDado1 - valorDado2;
        }
        if(opcionDosCorrecta){
            if(valor == resultado) {
                MediaPlayer mp = MediaPlayer.create(this, R.raw.bien);
                mp.start();
                return true;
            }
        }else{

            if(!opcionUnoCorrecta){//pone el alerta en donde corresponda durante dos segundos
                imagePrimerOperando.setBackgroundResource(R.drawable.recuadro_rojo_shape);
            }else {
                imageSegundoOperando.setBackgroundResource(R.drawable.recuadro_rojo_shape);
            }
        }
        MediaPlayer mp = MediaPlayer.create(this, R.raw.error);
        mp.start();
        return false;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        ClipData data = ClipData.newPlainText("", "");
        imagePrimerOperando.startDrag(data, new View.DragShadowBuilder(v), null, 0);
        imageSegundoOperando.startDrag(data, new View.DragShadowBuilder(v), null, 0);
        imageResultado.startDrag(data, new View.DragShadowBuilder(v), null, 0);
        v.setVisibility(View.INVISIBLE);
        imagenSeleccionada = (ImageView) v;
        return true;
    }

    public void completarOpciones(){
        int cantidad = 0;
        while(cantidad <2){
            int op = rnd.nextInt(12) +1;
            if(!opciones.contains(op)){
                opciones.add(op);
                cantidad++;
            }
        }
        Collections.sort(opciones);
    }

    //invoco este metodo solo si es una resta
    //valida el orden de los operandos
    public void validarMayorMenor() {
        boolean seguir = true;
        while (seguir) {
            if (valorDado1 < valorDado2) { //si es menor lo intercabio
                int aux = valorDado1;
                valorDado1 = valorDado2;
                valorDado2 = aux;
                seguir = false;
            } else {
                if (valorDado1 == valorDado2) {//si son iguales los recalculo
                    valorDado2 = rnd.nextInt(dadosVal.size()) + 1;
                }else{
                    seguir = false;
                }
            }
        }
    }
    public void cargarImagenes(){
        dadosVal.put(1, R.drawable.dado1);
        dadosVal.put(2, R.drawable.dado2);
        dadosVal.put(3, R.drawable.dado3);
        dadosVal.put(4, R.drawable.dado4);
        dadosVal.put(5, R.drawable.dado5);
        dadosVal.put(6, R.drawable.dado6);
        numerosVal.put(1, R.drawable.uno);
        numerosVal.put(2, R.drawable.dos);
        numerosVal.put(3, R.drawable.tres);
        numerosVal.put(4, R.drawable.cuatro);
        numerosVal.put(5, R.drawable.cinco);
        numerosVal.put(6, R.drawable.seis);
        numerosVal.put(7, R.drawable.siete);
        numerosVal.put(8, R.drawable.ocho);
        numerosVal.put(9, R.drawable.nueve);
        numerosVal.put(10, R.drawable.diez);
        numerosVal.put(11, R.drawable.once);
        numerosVal.put(12, R.drawable.doce);

        }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
