package ar.edu.unnoba.ppc2016;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import ar.edu.unnoba.ppc2016.juego.JuegoActivity;
import ar.edu.unnoba.ppc2016.tema1.SumaRestaActivity;
import ar.edu.unnoba.ppc2016.tema5.MayorMenorActivity;

public class MainActivity extends AppCompatActivity {

    Animation deslizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        Button buttonSumaResta = (Button) findViewById(R.id.sumaResta);
        Button buttonMayorMenor = (Button) findViewById(R.id.mayorMenor);
        Button buttonJuego = (Button) findViewById(R.id.juego);

        buttonSumaResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SumaRestaActivity.class);
                startActivity(intent);
            }
        });

        buttonMayorMenor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MayorMenorActivity.class);
                startActivity(intent);
            }
        });

        buttonJuego.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), JuegoActivity.class);
                startActivity(intent);
            }
        });

        deslizar = AnimationUtils.loadAnimation(this, R.anim.anim_main);
        buttonJuego.startAnimation(deslizar);
        buttonMayorMenor.startAnimation(deslizar);
        buttonSumaResta.startAnimation(deslizar);

    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
