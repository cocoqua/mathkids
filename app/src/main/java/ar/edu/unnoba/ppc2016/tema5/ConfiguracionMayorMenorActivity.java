package ar.edu.unnoba.ppc2016.tema5;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import ar.edu.unnoba.ppc2016.R;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class ConfiguracionMayorMenorActivity extends AppCompatActivity {

    private NumberPicker pickerMinimo;
    private NumberPicker pickerMaximo;
    private ArrayList<String> valores;
    private TextView error;
    private Button btnGuardar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion_mayor_menor);
        getSupportActionBar().setTitle(getString(R.string.configuracion));

        error = (TextView) findViewById(R.id.error);
        pickerMaximo = (NumberPicker) findViewById(R.id.pickerMaximo);
        pickerMinimo = (NumberPicker) findViewById(R.id.pickerMinimo);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);

        valores = new ArrayList<>();
        valores.add("1");
        valores.add("10");
        valores.add("20");
        valores.add("30");
        valores.add("40");
        valores.add("50");
        valores.add("60");
        valores.add("70");
        valores.add("80");
        valores.add("90");
        valores.add("100");



        pickerMinimo.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        pickerMaximo.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        String[] val = new String[valores.size()];
        val = valores.toArray(val);

        pickerMinimo.setMinValue(0);
        pickerMinimo.setMaxValue(valores.size()-1);
        pickerMaximo.setMinValue(0);
        pickerMaximo.setMaxValue(valores.size()-1);

        pickerMinimo.setDisplayedValues(val);
        pickerMaximo.setDisplayedValues(val);

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName),MODE_PRIVATE);
        Integer minimo = sharedPref.getInt(getString(R.string.configMinimo), 1);
        Integer maximo = sharedPref.getInt(getString(R.string.configMaximo), 20);

        pickerMinimo.setValue(valores.indexOf(minimo.toString()));
        pickerMaximo.setValue(valores.indexOf(maximo.toString()));

        pickerMinimo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                validar();
            }
        });

        pickerMaximo.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                validar();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getSharedPreferences(getString(R.string.prefName),MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(getString(R.string.configMaximo), Integer.valueOf(valores.get(pickerMaximo.getValue())));
                editor.putInt(getString(R.string.configMinimo), Integer.valueOf(valores.get(pickerMinimo.getValue())));
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), MayorMenorActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }



    public void validar(){
        String min = valores.get(pickerMinimo.getValue());
        String max = valores.get(pickerMaximo.getValue());
        if(Integer.parseInt(min) < Integer.parseInt(max)){
            error.setVisibility(View.INVISIBLE);
            btnGuardar.setEnabled(true);
            btnGuardar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            btnGuardar.setTextColor(Color.WHITE);
        }else {
            error.setVisibility(View.VISIBLE);
            btnGuardar.setEnabled(false);
            btnGuardar.setBackgroundColor(Color.GRAY);
            btnGuardar.setTextColor(Color.DKGRAY);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
