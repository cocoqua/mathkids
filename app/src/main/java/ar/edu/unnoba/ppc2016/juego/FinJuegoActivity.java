package ar.edu.unnoba.ppc2016.juego;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import ar.edu.unnoba.ppc2016.MainActivity;
import ar.edu.unnoba.ppc2016.R;

public class FinJuegoActivity extends AppCompatActivity {
    private static final int DIFICULTAD_FACIL = 1;
    private static final int DIFICULTAD_MEDIA = 2;
    private static final int DIFICULTAD_DIFICIL = 3;

    Button btnContinuar, btnSalir;
    TextView aciertos, puntos, errores, dificultad;
    LinearLayout layoutPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fin_juego);

        Typeface font = Typeface.createFromAsset(getAssets(), "Levator.ttf");

        layoutPrincipal = (LinearLayout) findViewById(R.id.layoutPrincipal);
        aciertos = (TextView) findViewById(R.id.aciertos);
        puntos = (TextView) findViewById(R.id.puntos);
        dificultad = (TextView) findViewById(R.id.dificultad);
        errores = (TextView) findViewById(R.id.errores);

        btnContinuar = (Button) findViewById(R.id.btnContinuar);
        btnSalir = (Button) findViewById(R.id.btnSalir);
        int puntaje = getIntent().getExtras().getInt("puntos");
        int puntajeMaximo = getIntent().getExtras().getInt("puntajeMaximo");
        int acierto = getIntent().getExtras().getInt("aciertos");
        int numeroPantallas = getIntent().getExtras().getInt("pantallas");
        int numDificultad = getIntent().getExtras().getInt("dificultad");

        aciertos.setText(String.valueOf(acierto));
        errores.setText(String.valueOf(numeroPantallas-acierto));
        puntos.setText(String.valueOf(puntaje));

        aciertos.setTypeface(font);
        puntos.setTypeface(font);
        errores.setTypeface(font);

        switch (numDificultad){
            case DIFICULTAD_FACIL:
                dificultad.setText("DIFICULTAD: Facil");
                break;
            case DIFICULTAD_MEDIA:
                dificultad.setText("DIFICULTAD: Media");
                break;
            case DIFICULTAD_DIFICIL:
                dificultad.setText("DIFICULTAD: Dificil");
                break;
        }

        MediaPlayer mp;
        if (puntaje > puntajeMaximo){
            layoutPrincipal.setBackgroundResource(R.drawable.fondo_puntaje_maximo);
            mp = MediaPlayer.create(this, R.raw.audio_puntaje_maximo);
        }else{
            layoutPrincipal.setBackgroundResource(R.drawable.fondo_final);
            mp = MediaPlayer.create(this, R.raw.audio_fin_del_juego);
        }
        mp.start();



        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), JuegoActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
